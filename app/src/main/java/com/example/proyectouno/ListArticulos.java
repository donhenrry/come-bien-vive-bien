package com.example.proyectouno;

public class ListArticulos {
    private String nombrep,descripcion;
    private int codigop;
    private double precio;
    private long id;


    public ListArticulos(int codigop,String nombrep,String descripcion,double precio) {
        this.nombrep = nombrep;
        this.descripcion = descripcion;
        this.codigop = codigop;
        this.precio = precio;
    }
    public ListArticulos(int codigop,String nombrep,String descripcion,double precio,long id){
        this.nombrep = nombrep;
        this.descripcion = descripcion;
        this.codigop = codigop;
        this.precio = precio;

        this.id=id;

    }

    public String getNombrep() {
        return nombrep;
    }

    public void setNombrep(String nombrep) {
        this.nombrep = nombrep;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigop() {
        return codigop;
    }

    public void setCodigop(int codigop) {
        this.codigop = codigop;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ListArticulos{" +
                "nombrep='" + nombrep + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", codigop=" + codigop +
                ", precio=" + precio +
                ", id=" + id +
                '}';
    }
}

