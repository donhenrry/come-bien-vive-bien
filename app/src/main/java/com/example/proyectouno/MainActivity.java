package com.example.proyectouno;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity {

    Button btnRegresar,btnlogin;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnRegresar = findViewById(R.id.btnlogin);
        //Transformamos los datos de TextInput a TextView

        TextView nombre = (TextView) findViewById(R.id.nombre);
        TextView pass = (TextView) findViewById(R.id.pass);

        Button btnlogin = (Button) findViewById(R.id.btnlogin2);



        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                if(nombre.getText().toString().equals("jose") && pass.getText().toString().equals("123")){
                    // Mensajes en Android (JAVA)
                    Toast.makeText(MainActivity.this,"Usuario registrado", Toast.LENGTH_LONG).show();
                }else
                    Toast.makeText(MainActivity.this, "Usuario no registrado", Toast.LENGTH_LONG).show();


            }
        });

    }

    public void form(View v){
        Intent cambio = new Intent(MainActivity.this, Formulario.class);
        startActivity(cambio);

    }
}
