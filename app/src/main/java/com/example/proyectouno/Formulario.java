package com.example.proyectouno;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proyectouno.persistencia.DbUsuarios;

public class Formulario extends AppCompatActivity {
    EditText nombre, apellidos, email, password;
    Button btnRegistro, btnVolver;
    DbUsuarios DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        nombre = findViewById(R.id.nombre);
        apellidos = findViewById(R.id.apellidos);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btnRegistro = findViewById(R.id.btnRegistro);
        btnVolver = findViewById(R.id.btnVolver);
        DB = new DbUsuarios(this);




        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nombre.getText().toString(); //
                String lastname = apellidos.getText().toString();
                String correo = email.getText().toString();
                String contrasena = password.getText().toString();

                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(lastname) || TextUtils.isEmpty(correo) || TextUtils.isEmpty(contrasena))
                    Toast.makeText(Formulario.this, "Se requiere llenar los campos", Toast.LENGTH_SHORT).show();
                else {
                    Boolean checkuser = DB.checknomusuario(name);
                    if (checkuser == false) {
                        Boolean insert = DB.insertarUsuario(name, lastname, correo, contrasena);
                        if (insert == true) {
                            Toast.makeText(Formulario.this, "Registrado correctamente", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);

                        } else {
                            Toast.makeText(Formulario.this, "Registro fallido", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Formulario.this, "El usuario ya existe", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);

            }
        });

    }
}