package com.example.proyectouno.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=3; // Cambia/aumenta la versión cuando añadimos una tabla o una columna a nuestra base de datos
    private static final String DATABASE_NOMBRE = "proyecto.db";
    public static final String TABLE_USERS = "usuarios";
    public static final String TABLE_PRODUCTOS = "productos";


    public DbHelper(Context context) {
        super(context, DATABASE_NOMBRE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_USERS + "("+
                "nombre INTEGER PRIMARY KEY AUTOINCREMENT," +
                "apellidos TEXT NOT NULL,"+
                "email TEXT NOT NULL,"+
                "password TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS " + TABLE_USERS);// Consulta
        onCreate(sqLiteDatabase);


    }

}






